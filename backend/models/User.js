import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let User = new Schema({
    imie: {
        type: String
    },
    nazwisko: {
        type: String
    },
    login: {
        type: String
    },
    haslo: {
        type: String
    },
    uprawienia: {
        type: String,
        default: 'user'
    }
});

export default mongoose.model('User', User);