import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let UserLOG = new Schema({
    imie: {
        type: String
    },
    nazwisko: {
        type: String
    },
    login: {
        type: String
    },
    haslo: {
        type: String
    },
    uprawienia: {
        type: String
    },
    id_user:{
        type: String
    }
});

export default mongoose.model('UserLOG', UserLOG);