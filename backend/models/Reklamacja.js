import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Reklamacja = new Schema({
    tytul: {
        type: String
    },
    data: {
        type: String
    },
    opis: {
        type: String
    },
    priorytet: {
        type: String
    },
    status: {
        type: String,
        default: 'otwarte'
    },
    id_user: {
        type: String
    }
});

export default mongoose.model('Reklamacja', Reklamacja);