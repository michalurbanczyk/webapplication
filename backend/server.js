import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

import Issue from './models/Issue';
import User from './models/User'; //dopisane
import UserLOG from './models/User_session';
import Reklamacja from './models/Reklamacja';

const app = express();
const router = express.Router();

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/issues');
const connection = mongoose.connection;


connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});

router.route('/userslog').get((req,res) => {
    UserLOG.find((err, users) => {
        if(err)
            console.log(err);
        else
            res.json(users);
    });
});

router.route('/userslog/add').post((req, res) => {
    let userLOG = new UserLOG(req.body);
    userLOG.save()
        .then(user => {
            res.status(200).json({'user': 'Added successfully, Session start!'});
        })
        .catch(err => {
            res.status(400).send('Failed to create new record');
        });
});

router.route('/userslog/delete/:id').get((req, res) => {
    UserLOG.findByIdAndRemove({_id: req.params.id}, (err, user) => {
        if (err)
            res.json(err);
        else
            res.json('Remove successfully');
    })
});

router.route('/users').get((req,res) => {
    User.find((err, users) => {
        if(err)
            console.log(err);
        else
            res.json(users);
    });
});

router.route('/users/add').post((req, res) => {
    let user = new User(req.body);
    user.save()
        .then(user => {
            res.status(200).json({'user': 'Added successfully'});
        })
        .catch(err => {
            res.status(400).send('Failed to create new record');
        });
});

router.route('/users/log').post((req,res) => {
    let userLog = new User(req.body);
    User.find({login:userLog.login,haslo:userLog.haslo}, (err,result) => {
        if(err)
            console.log(err);
        else
            res.json(result);
    });
});

router.route('/users/:id').get((req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (err)
            console.log(err);
        else
            res.json(user);
    });
});

router.route('/users/update/:id').post((req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (!user)
            return next(new Error('Could not load document'));
        else {
            user.imie = req.body.imie;
            user.nazwisko = req.body.nazwisko;
            user.login = req.body.login;
            user.haslo = req.body.haslo;
            user.uprawienia = req.body.uprawienia;

            user.save().then(user => {
                res.json('Update done');
            }).catch(err => {
                res.status(400).send('Update failed');
            });
        }
    });
});

router.route('/users/delete/:id').get((req, res) => {
    User.findByIdAndRemove({_id: req.params.id}, (err, user) => {
        if (err)
            res.json(err);
        else
            res.json('Remove successfully');
    })
})

router.route('/reklamacje').get((req, res) => {
    Reklamacja.find((err, dane) => {
        if (err)
            console.log(err);
        else
            res.json(dane);
    });
});

router.route('/reklamacje/:id').get((req, res) => {
    Reklamacja.findById(req.params.id, (err, dane) => {
        if (err)
            console.log(err);
        else
            res.json(dane);
    });
});

router.route('/reklamacje/add').post((req, res) => {
    let reklamacja = new Reklamacja(req.body);
    reklamacja.save()
        .then(reklamacja => {
            res.status(200).json({'reklamacja': 'Added successfully'});
        })
        .catch(reklamacja => {
            res.status(400).send('Failed to create new record');
        });
});

router.route('/reklamacje/update/:id').post((req, res) => {
    Reklamacja.findById(req.params.id, (err, dane) => {
        if (!dane)
            return next(new Error('Could not load document'));
        else {
            dane.tytul = req.body.tytul;
            dane.data = req.body.data;
            dane.opis = req.body.opis;
            dane.priorytet = req.body.priorytet;
            dane.status = req.body.status;
            dane.id_user = req.body.id_user;

            dane.save().then(dane => {
                res.json('Update done');
            }).catch(err => {
                res.status(400).send('Update failed');
            });
        }
    });
});

router.route('/reklamacje/delete/:id').get((req, res) => {
    Reklamacja.findByIdAndRemove({_id: req.params.id}, (err, dane) => {
        if (err)
            res.json(err);
        else
            res.json('Remove successfully');
    })
})


router.route('/reklamacje/poid').post((req,res) => {
    let reklamacja = new Reklamacja(req.body);
    Reklamacja.find({id_user:reklamacja.id_user}, (err,result) => {
        if(err)
            console.log(err);
        else
            res.json(result);
    });
});



app.use('/', router);

app.listen(4000, () => console.log('Express server running on port 4000'));