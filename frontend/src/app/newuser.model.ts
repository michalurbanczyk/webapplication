export interface User {
    id: String;
    imie: String;
    nazwisko: String;
    login: String;
    haslo: String;
    uprawienia: String;
}