import { Component } from '@angular/core';
import { NewuserService } from './newuser.service';
import { User } from './newuser.model';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  aktywnyButton: boolean = true;
  butA: boolean = true;
  butU: boolean = true;
  x : any = [{}];

  constructor(private service: NewuserService, private snackBar: MatSnackBar)
  {
    this.service.checkSession().subscribe((data : User) => {
      if(Object.keys(data).length > 0)
      {
        console.log("Sesja wykryta odtwarzam uzytkownika!");
        const user = {
          _id: Object.values(data)[0].id_user,
          imie: Object.values(data)[0].imie,
          nazwisko: Object.values(data)[0].nazwisko,
          login: Object.values(data)[0].login,
          haslo: Object.values(data)[0].haslo,
          uprawienia: Object.values(data)[0].uprawienia
        };
        this.service.sessionID = Object.values(data)[0]._id;
        this.x[0] = user;
        this.service.uzytkownik = this.x;
        this.service.czyZalogowanyStatus = true;
        this.snackBar.open('Sesja użytkownika odzyskana!','OK', {
          duration: 5000
        });
        if( Object.values(data)[0].uprawienia == 'admin') 
        {
          console.log("Uprawnienia admin nadane!");
          this.service.czyAdmin = true;
        }
        this.wylogujButton();
      }
      else
      {
        console.log("Brak sesji!");
      }
    });    
  }

  wylaczB()
  {
    this.service.delSession(this.service.sessionID).subscribe( () => {
      this.snackBar.open('Wylogowano pomyślnie!','OK', {
        duration: 5000
      });
      this.service.czyAdmin = false;
      this.service.czyZalogowanyStatus = false;
      this.service.uzytkownik = null;
      this.wylogujButton();
    });
  }

  wylogujButton ()
  {
    if(this.service.czyZalogowanyStatus)
    {
      this.aktywnyButton = false;
      if(this.service.czyAdmin)this.butA = false;
      this.butU = false;
    }
    else
    {
      this.aktywnyButton = true;
      this.butA = true;
      this.butU = true;
    }
  }

}
