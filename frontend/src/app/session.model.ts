export interface Obiekt {
    id: String;
    imie: String;
    nazwisko: String;
    login: String;
    haslo: String;
    uprawienia: String;
    id_user: String;
}