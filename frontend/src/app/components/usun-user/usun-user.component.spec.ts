import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsunUserComponent } from './usun-user.component';

describe('UsunUserComponent', () => {
  let component: UsunUserComponent;
  let fixture: ComponentFixture<UsunUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsunUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsunUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
