import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { User } from '../../newuser.model';
import { NewuserService } from '../../newuser.service';
import { MatSnackBar } from '@angular/material';

import { Reklamacja } from '../../reklamacja.model';

@Component({
  selector: 'app-adminpage',
  templateUrl: './adminpage.component.html',
  styleUrls: ['./adminpage.component.css']
})
export class AdminpageComponent implements OnInit {

  reklamacje : Reklamacja [];
  wszyscyUser: User[];
  currentUser: User;
  displayedColumns = ['imie', 'nazwisko', 'login', 'haslo', 'uprawienia', 'id', 'actions'];
  displayedColumns2 = ['tytul', 'data', 'priorytet', 'id_user', 'status', 'actions'];

  constructor(private usluga: NewuserService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.uzupelnijUser();
    this.uzupelnijReklam();
    this.currentUser = this.usluga.uzytkownik;
  }

  uzupelnijUser()
  {
    this.usluga
      .getAllUser()
      .subscribe((data: User[]) => {
        this.wszyscyUser = data;
      });
  }

  uzupelnijReklam()
  {
    this.usluga.getAllReklam().subscribe((data: Reklamacja[]) => {
      this.reklamacje = data;
    });
  }

  edytujUser(id)
  {
    this.router.navigate([`/edycja-user/${id}`]);
  }

  usunUser(id)
  {
    this.usluga.deleteUser(id).subscribe(() => {
      this.uzupelnijUser();
      this.snackBar.open('Użytkownik usuniety pomyślnie!','OK', {
        duration: 3000
      });
    });
  }

  edytujReklam(id)
  {
    this.router.navigate([`/editreklamacje/${id}`]);
  }

  usunReklam(id)
  {
    this.usluga.deleteReklam(id).subscribe( () => {
      this.uzupelnijReklam();
      this.snackBar.open('Reklamacja usunięta pomyślnie!','OK', {
        duration: 5000
      });
    });
  }

}
