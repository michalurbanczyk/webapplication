import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NewuserService } from '../../newuser.service';
import { User } from '../../newuser.model'
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-logowanie',
  templateUrl: './logowanie.component.html',
  styleUrls: ['./logowanie.component.css']
})
export class LogowanieComponent implements OnInit {

  logUser: FormGroup;
  
  constructor(private newuserService: NewuserService, private fb: FormBuilder, private router: Router, private wylogujB: AppComponent) {
    this.logUser = this.fb.group({
      login: ['', Validators.required],
      haslo: ['', Validators.required]
    });
  }

  loginUser(login,haslo){
    this.newuserService.loginUser(login,haslo).subscribe((data: User) => {
      if(Object.keys(data).length > 0)
      {
        console.log("Znaleziono użytkownika!");
        this.newuserService.uzytkownik = data;
        this.newuserService.addSession(Object.values(data)[0]._id,Object.values(data)[0].imie,Object.values(data)[0].nazwisko,Object.values(data)[0].login,Object.values(data)[0].haslo,Object.values(data)[0].uprawienia).subscribe( () => {
          this.newuserService.checkSession().subscribe((data : User ) => {
            this.newuserService.sessionID = data[0]._id;
          })
        })

        window.alert('Zalogowano pomyslnie!');
        this.newuserService.czyZalogowanyStatus = true;
        
        if( Object.values(data)[0].uprawienia == 'admin') 
        {
          this.newuserService.czyAdmin = true;
          this.router.navigate([`/adminpage`]);
        }
        else
        {
          this.router.navigate([`/userpage`]);
        }
        this.wylogujB.wylogujButton();
        
        
      }
      else
      {
        window.alert('Błędny login lub haslo'); 
      }
    });
  }

  ngOnInit() {
  }

}
