import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material';
import { NewuserService } from '../../newuser.service';

@Component({
  selector: 'app-edycja-user',
  templateUrl: './edycja-user.component.html',
  styleUrls: ['./edycja-user.component.css']
})
export class EdycjaUserComponent implements OnInit {

  id: String;
  user: any = {};
  updateForm: FormGroup;

  constructor(private serwis: NewuserService, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar, private fb: FormBuilder) 
  {
    this.tworzenieForm();
  }

  tworzenieForm()
  {
    this.updateForm = this.fb.group({
      imie: ['', Validators.required],
      nazwisko: ['', Validators.required],
      login: ['', Validators.required],
      haslo: ['', Validators.required],
      uprawienia: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.serwis.getOneUser(this.id).subscribe(res => {
        this.user = res;
        this.updateForm.get('imie').setValue(this.user.imie);
        this.updateForm.get('nazwisko').setValue(this.user.nazwisko);
        this.updateForm.get('login').setValue(this.user.login);
        this.updateForm.get('haslo').setValue(this.user.haslo);
        this.updateForm.get('uprawienia').setValue(this.user.uprawienia)
      });
    });
  }

  aktualizacjaUser(imie,nazwisko,login,haslo,uprawienia)
  {
    this.serwis.updateUser(this.id,imie,nazwisko,login,haslo,uprawienia).subscribe(() => {
      this.snackBar.open('Użytkownik zaktualizowany!','OK', {
        duration: 3000
      });
    });
  }

}
