import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdycjaUserComponent } from './edycja-user.component';

describe('EdycjaUserComponent', () => {
  let component: EdycjaUserComponent;
  let fixture: ComponentFixture<EdycjaUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdycjaUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdycjaUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
