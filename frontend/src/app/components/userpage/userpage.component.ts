import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

import { User } from '../../newuser.model';
import { NewuserService } from '../../newuser.service';
import { Reklamacja } from '../../reklamacja.model';


@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit {

  uzytkownik: User;
  reklamacje: Reklamacja[];
  displayedColumns = ['tytul', 'data', 'priorytet', 'status'];

  constructor(private router: Router, private userStatus: NewuserService) {
    this.uzytkownik = userStatus.uzytkownik;
   }

  ngOnInit() {
    this.refreshLista();
  }

  refreshLista()
  {
    this.userStatus.getListReklam(this.uzytkownik[0]._id).subscribe((data : Reklamacja[]) => {
      this.reklamacje = data;
    });
  }

}
