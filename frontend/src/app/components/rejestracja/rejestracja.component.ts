import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NewuserService } from '../../newuser.service';

@Component({
  selector: 'app-rejestracja',
  templateUrl: './rejestracja.component.html',
  styleUrls: ['./rejestracja.component.css']
})
export class RejestracjaComponent implements OnInit {

  createUser: FormGroup;

  constructor(private newuserService: NewuserService, private fb: FormBuilder, private router: Router) {
    this.createUser = this.fb.group({
      imie: ['', Validators.required],
      nazwisko: ['', Validators.required],
      login: ['', Validators.required],
      haslo: ['', Validators.required]
    });
  }

  addUser(imie,nazwisko,login,haslo){
    this.newuserService.addUser(imie,nazwisko,login,haslo).subscribe(() => {
      window.alert('Konto utworzono pomyslnie!');
      this.router.navigate(['/main']);
    })
  }

  ngOnInit() {
  }

}
