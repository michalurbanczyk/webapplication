import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditreklamacjeComponent } from './editreklamacje.component';

describe('EditreklamacjeComponent', () => {
  let component: EditreklamacjeComponent;
  let fixture: ComponentFixture<EditreklamacjeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditreklamacjeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditreklamacjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
