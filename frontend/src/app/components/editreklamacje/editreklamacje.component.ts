import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material';
import { NewuserService } from '../../newuser.service';

@Component({
  selector: 'app-editreklamacje',
  templateUrl: './editreklamacje.component.html',
  styleUrls: ['./editreklamacje.component.css']
})
export class EditreklamacjeComponent implements OnInit {

  id: String;
  reklamacja: any = {};
  updateForm: FormGroup;

  constructor(private serwis: NewuserService, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar, private fb: FormBuilder) {
    this.tworzenieForm();
  }

  tworzenieForm()
  {
    this.updateForm = this.fb.group({
      tytul: ['', Validators.required],
      data: ['', Validators.required],
      opis: ['', Validators.required],
      priorytet: ['', Validators.required],
      id_user: ['', Validators.required],
      status: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.serwis.getOneReklam(this.id).subscribe(res => {
        this.reklamacja = res;
        this.updateForm.get('tytul').setValue(this.reklamacja.tytul);
        this.updateForm.get('data').setValue(this.reklamacja.data);
        this.updateForm.get('opis').setValue(this.reklamacja.opis);
        this.updateForm.get('priorytet').setValue(this.reklamacja.priorytet);
        this.updateForm.get('id_user').setValue(this.reklamacja.id_user);
        this.updateForm.get('status').setValue(this.reklamacja.status);
      });
    });
  }

  aktualizacjaReklam(tytul, data, opis, priorytet, id_user, status)
  {
    this.serwis.updateReklam(this.id,tytul, data, opis, priorytet, id_user, status).subscribe(() => {
      this.snackBar.open('Reklamacja zaktualizowana!','OK', {
        duration: 5000
      });
    });
  }

}
