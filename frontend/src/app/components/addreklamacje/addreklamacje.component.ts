import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'; 
import { NewuserService } from 'src/app/newuser.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-addreklamacje',
  templateUrl: './addreklamacje.component.html',
  styleUrls: ['./addreklamacje.component.css']
})
export class AddreklamacjeComponent implements OnInit {

  createForm: FormGroup;

  constructor(private serwis: NewuserService, private fb: FormBuilder, private router: Router, private snackBar: MatSnackBar ) {
    this.createForm = this.fb.group({
      tytul: ['', Validators.required],
      data: ['', Validators.required],
      opis: ['', Validators.required],
      priorytet: ['', Validators.required]
    });
  }

  addReklam(tytul,data,opis,priorytet)
  {
    this.serwis.addReklam(tytul,data,opis,priorytet,this.serwis.uzytkownik[0]._id).subscribe(() => {
      this.snackBar.open('Reklamacja dodana pomyślnie!','OK', {
        duration: 5000
      });
      this.router.navigate(['/userpage']);
    });
  }

  ngOnInit() {
  }

}
