import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddreklamacjeComponent } from './addreklamacje.component';

describe('AddreklamacjeComponent', () => {
  let component: AddreklamacjeComponent;
  let fixture: ComponentFixture<AddreklamacjeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddreklamacjeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddreklamacjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
