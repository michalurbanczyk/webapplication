import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NewuserService } from './newuser.service';
import { User } from './newuser.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor (private logowanie: NewuserService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(!this.logowanie.czyZalogowanyStatus)
      {
        this.router.navigate([`/main`]);
      }

    return this.logowanie.czyZalogowanyStatus;
  }
}
