export interface Reklamacja {
    id: String;
    tytul: String;
    data: String;
    opis: String;
    priorytet: String;
    id_user: String;
    status: String;
}