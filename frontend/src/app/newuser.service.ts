import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './newuser.model';



@Injectable({
  providedIn: 'root'
})
export class NewuserService {

  baza = 'http://localhost:4000';

  czyZalogowanyStatus = false;
  czyAdmin = false;
  uzytkownik : User;
  sessionID : String;

  constructor(private http: HttpClient) { }

  getListReklam(id)
  {
    const id_user = {
      id_user: id
    };
    return this.http.post(`${this.baza}/reklamacje/poid`, id_user);
  }

  getAllReklam()
  {
    return this.http.get(`${this.baza}/reklamacje`);
  }

  getOneReklam(id)
  {
    return this.http.get(`${this.baza}/reklamacje/${id}`);
  }

  updateReklam(id,tytul,data,opis,priorytet,id_user,status) {
    const reklamacja = {
      tytul: tytul,
      data: data,
      opis: opis,
      priorytet: priorytet,
      id_user: id_user,
      status: status
    };
    return this.http.post(`${this.baza}/reklamacje/update/${id}`, reklamacja);
  }

  deleteReklam(id) {
    return this.http.get(`${this.baza}/reklamacje/delete/${id}`);
  }


  addReklam(tytul,data,opis,priorytet,id_user)
  {
    const reklamacja = {
      tytul: tytul,
      data: data,
      opis: opis,
      priorytet: priorytet,
      id_user: id_user
    };
    return this.http.post(`${this.baza}/reklamacje/add`, reklamacja);
  }

  checkSession()
  {
    return this.http.get(`${this.baza}/userslog`);
  }

  addSession(id,imie,nazwisko,login,haslo,uprawienia)
  {
    const session = {
      imie: imie,
      nazwisko: nazwisko,
      login: login,
      haslo: haslo,
      uprawienia: uprawienia,
      id_user: id
    };
    return this.http.post(`${this.baza}/userslog/add`, session);
  }

  delSession(id)
  {
    return this.http.get(`${this.baza}/userslog/delete/${id}`);
  }

  addUser(imie,nazwisko,login,haslo) {
    const user = {
      imie: imie,
      nazwisko: nazwisko,
      login: login,
      haslo: haslo
    };
    return this.http.post(`${this.baza}/users/add`, user);
  }

  loginUser(login,haslo) {
    const user = {
      login: login,
      haslo: haslo
    };
    return this.http.post(`${this.baza}/users/log`, user);
  }

  updateUser(id, imie, nazwisko, login, haslo, uprawienia) {
    const user = {
      imie: imie,
      nazwisko: nazwisko,
      login: login,
      haslo: haslo,
      uprawienia: uprawienia
    };
    return this.http.post(`${this.baza}/users/update/${id}`, user);
  }

  deleteUser(id) {
    return this.http.get(`${this.baza}/users/delete/${id}`);
  }

  getAllUser()
  {
    return this.http.get(`${this.baza}/users`);
  }

  getOneUser(id)
  {
    return this.http.get(`${this.baza}/users/${id}`);
  }
}
