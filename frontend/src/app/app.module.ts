import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MatToolbarModule, MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatIconModule, MatButtonModule, MatCardModule, MatTableModule, MatDividerModule, MatSnackBarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';


import { IssueService } from './issue.service';
import { MainComponent } from './components/main/main.component';
import { LogowanieComponent } from './components/logowanie/logowanie.component';
import { RejestracjaComponent } from './components/rejestracja/rejestracja.component';
import { NewuserService } from './newuser.service';
import { AdminpageComponent } from './components/adminpage/adminpage.component';
import { UserpageComponent } from './components/userpage/userpage.component';
import { AuthGuard } from './auth.guard';
import { AuthAdminGuard } from './auth-admin.guard';
import { EdycjaUserComponent } from './components/edycja-user/edycja-user.component';
import { UsunUserComponent } from './components/usun-user/usun-user.component';
import { LogowanieAuthGuard } from './logowanie-auth.guard';
import { AddreklamacjeComponent } from './components/addreklamacje/addreklamacje.component';
import { EditreklamacjeComponent } from './components/editreklamacje/editreklamacje.component';

const routes: Routes = [
  { path: 'create', component: CreateComponent},
  { path: 'edit/:id', component: EditComponent},
  { path: 'list', component: ListComponent},
  { path: 'main', component: MainComponent,},
  { path: 'logowanie', component: LogowanieComponent, canActivate: [LogowanieAuthGuard] },
  { path: 'rejestracja', component: RejestracjaComponent, canActivate: [LogowanieAuthGuard]},
  { path: 'userpage', component: UserpageComponent, canActivate: [ AuthGuard ] },
  { path: 'adminpage', component: AdminpageComponent, canActivate: [ AuthAdminGuard ] },
  { path: 'usun-user', component: UsunUserComponent},
  { path: 'edycja-user/:id', component: EdycjaUserComponent},
  { path: 'addreklamacje', component: AddreklamacjeComponent},
  { path: 'editreklamacje/:id', component: EditreklamacjeComponent},
  { path: '', redirectTo: 'main', pathMatch: 'full',}
];

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    MainComponent,
    LogowanieComponent,
    RejestracjaComponent,
    AdminpageComponent,
    UserpageComponent,
    EdycjaUserComponent,
    UsunUserComponent,
    AddreklamacjeComponent,
    EditreklamacjeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatDividerModule,
    MatSnackBarModule
  ],
  providers: [IssueService, NewuserService, AuthGuard, AuthAdminGuard, LogowanieAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
