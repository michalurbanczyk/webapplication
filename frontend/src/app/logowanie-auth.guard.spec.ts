import { TestBed, async, inject } from '@angular/core/testing';

import { LogowanieAuthGuard } from './logowanie-auth.guard';

describe('LogowanieAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogowanieAuthGuard]
    });
  });

  it('should ...', inject([LogowanieAuthGuard], (guard: LogowanieAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
