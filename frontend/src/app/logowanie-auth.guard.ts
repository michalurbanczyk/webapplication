import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot , Router} from '@angular/router';
import { Observable } from 'rxjs';
import { NewuserService } from './newuser.service';

@Injectable({
  providedIn: 'root'
})
export class LogowanieAuthGuard implements CanActivate {

  constructor (private logowanie: NewuserService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(this.logowanie.czyZalogowanyStatus)
      {
        if(this.logowanie.czyAdmin)
        {
          this.router.navigate([`/adminpage`]);
        }
        else
        {
          this.router.navigate([`/userpage`]);
        }
        return false;
      }
      else
      {
        return true;
      }
  }
}
