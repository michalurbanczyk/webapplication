import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NewuserService } from './newuser.service';

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuard implements CanActivate {

  constructor (private logowanieAdmina: NewuserService, private router : Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(!this.logowanieAdmina.czyAdmin)
      {
        this.router.navigate([`/main`]);
      }
    return this.logowanieAdmina.czyAdmin;
  }
}
